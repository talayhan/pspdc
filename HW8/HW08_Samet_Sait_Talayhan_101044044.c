/*################################################################*/
/* HW08_Samet_Sait_Talayhan_101044044.c   		                  */
/* ----------------------                                         */
/* Created on May 5, 2013, 1:52 PM by Samet Sait Talayhan.        */ 
/*                                                                */
/* Description                                                    */
/* ----------------------                                         */
/* This file implements PART2, PART3, and some functions of PART1 */ 
/* ----------------------                                         */
/*                                                                */
/*################################################################*/

/*################################################################*/
/*                           Includes                             */
/*################################################################*/
#include <stdio.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

/*
* Locate first occurrence of character in string.
* -----------------------------------------------
* Returns a pointer to the first occurence of character in the C
* string str. The terminating NULL-character is considered part of
* the C string. Therefore, it can also be located in order to 
* retrieve a pointer to the end of a string.
*/
char *
strChr(char *str, int chr){
	if (str == NULL)
	{
		return NULL;
	}
	else if ( (int)str[0] == chr )
	{
		return &str[0];
	}
	else
		return strChr( str+1, chr );
}
/*
* Locate last occurrence of character in string.
* -----------------------------------------------
* Returns a pointer to the last occurence of character in the C
* string str. The terminating NULL-character is considered part of
* the C string. Therefore, it can also be located in order to 
* retrieve a pointer to the end of a string.
*/
char *
strRChr(char *str, int chr){
	int length = strlen(str);
	if (str == NULL)
	{
		return NULL;
	}
	else if ( (int)str[length-1] == chr )
	{
		return &str[length-1];
	}
	else{
		str[length-1] = '\0';
		return strRChr( str, chr );
	}
}

/*
* This implementation recursive to calculate combination.
*
**/
int
getCombinationRec(int n, int k){
	if ( n >= 0 )
	{
		if ( k == 0 )
		{
			return 1;
		}
	}
	if ( k > 0 )
	{
		if ( n == 0 )
		{
			return 0;
		}
	}
	
	return getCombinationRec(n-1,k-1) + getCombinationRec(n-1,k);
}
/*
* This implementation iterative to calculate combination.
*
**/
int
getCombinationIter(int n, int k){
	return calculateFactorial(n) /
			( calculateFactorial(k) * calculateFactorial(n-k) );
}
/*
* This function calculate factorial the given number.
*/
int
calculateFactorial(int n){
	int i = 1, result = 1;

	if (n == 0 || n == 1)
	{
		return 1;
	}

	for ( i = 1; i <= n; ++i)
	{
		result *= i;
	}
	return result;
}
/*
* Determines if set is empty. If so, returns 1; if not, returns 0.
*/
int
is_empty(const char *set)
{
	return (set[0] == '\0');
}
/*
* Determines if ele is an element of set.
*/
int
is_element(char ele,	/* input - element to look for in set */
	 const char *set)	/* input - set in which to look for ele */
{
	int ans = FALSE, i = 1;
	int strLen = strlen(set);
	if (is_empty(set))
	{
		ans = FALSE;
		return ans;
	}
	while(i < strLen)
	{
		if (set[i] == ele)
			ans = TRUE;
		++i;
	}
	
	return (ans);
}

/*
* Determines if string value of set represents a valid set (no duplicate
* elements)
*/
int
is_set (const char *set)
{
	int ans = TRUE;
	int i,j;
	int strLen = strlen(set);

	if (is_empty(set))
		ans = TRUE;

	while(i<strLen){
		j = i + 1;
		while(j<strLen){
			if(set[i] == set[j])
				ans = FALSE;
			j++; 
		}
		i++;
	}

	return (ans);
}
/*################################################################*/
/* int main() Driver Function, Test above functions.
/* ----------                                                     */
/* Return                                                         */
/* ----------                                                     */
/*0 on success                                                    */
/*################################################################*/
int
main(int argc, char const *argv[])
{
	int testCombination;
	char str[] = "This is a sample string";
	char *pch;
	printf ("Looking for the 's' character in \"%s\" with strChr\n",str);
	/* Test strChr function */
	pch=strChr(str,'s');
	printf ("found at %d\n",pch-str+1);
	
	/* Test strRChr function */
	printf ("Looking for the 's' character in \"%s\" with strRChr\n",str);
	pch=strRChr(str,'s');
	printf ("found at %d\n",pch-str+1);
	
	/* Test getCombinationIter */
	printf("Test getCombinationIter(5,2) Function: %d\n",
										getCombinationIter(5,2));
	
	/* Test getCombinationRec */
	printf("Test getCombinationRec(5,2) Function: %d\n",
										getCombinationRec(5,2));

	/* Not testing set functions. */
	
	return 0;
}
/*################################################################*/
/*        End of HW08_Samet_Sait_Talayhan_101044044.c             */
/*################################################################*/

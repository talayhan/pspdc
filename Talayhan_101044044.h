/* 
 * File:   Talayhan_101044044.h
 * Author: neo
 *
 * Created on May 19, 2013, 12:31 PM
 */

#ifndef TALAYHAN_101044044_H
#define	TALAYHAN_101044044_H

/*################################################################*/
/*                           Defines                              */
/*################################################################*/
#define DEBU

#define TRUE 1
#define FALSE 0

#define MAX_NAME_SIZE 50
#define MAX_UNI_CAPACITY 100
#define MAX_DEP_CAPACITY 90
#define MAX_STUDENT_CAPACITY 22000
#define PREFENCES_SIZE 8

#define STUDENT_BIN "StudentDataBase.bin"
#define UNIVERSITY_BIN "UniversityDataBase.bin"

typedef char String[MAX_NAME_SIZE];

typedef struct {
	int day;
	int month;
	int year;
} birthday_t;

typedef struct{
	String eMail;
	String address;
	long long int phone;
} address_t;

typedef struct {
	int preferencesUniID[PREFENCES_SIZE];
	int preferencesDepID[PREFENCES_SIZE];
	/* If placement UniID is -1, student is not be placement */
	int placementUniID;	
	int placementDepID;
        int rank;
} prefences_t;

typedef struct {
	long long int id;
	String name;
	String sirName;
	char scoreType;		/* Quantative or Verbal */
	birthday_t birthday;
	double quantativeScore;
	double verbalScore;
	address_t address;
	prefences_t preference;
        int flag; /* for DELETION operation */
} student_t;

typedef struct {
	String departmentName;
	int depID;
	char scoreType;
	int quota;
        int size;
	int flag; /* DELETION 0 - 1 if is 0 not suitable to use */
} department_t;

typedef struct {
	String name;
	int uniID;
	department_t departments[MAX_DEP_CAPACITY];
        int departmentSize;
        int flag;
} universty_t;
int
readDepartments(FILE *inp, universty_t *university);

void
readStudentsAddressFromFile(FILE *inputStudentAddress, student_t *students, 
        int studentSize);

void
readStudentsNamesFromFile(FILE *inputStudentName, student_t *students, 
        int studentSize);

int
readStudentsScoresFromFile(FILE *inputStudentsScore, student_t *students, 
        int studentSize);

void
readStudentsPreferencesFromFile(FILE *inputStudentsPreferences,
        student_t *students);

int
isStudent(student_t *students, long long int target, int studentSize);

int
isUniversity(universty_t *universties, int uniID, int uniSize);

void
displayUniversities(universty_t *universities, int universitiesSize);

void
displayStudents(student_t *student, int studentSize, universty_t *universities);

void
displayPlacementStatisticsOfDeparments(universty_t *universities, int uniSize);

void
sortingStudentQuantative(student_t *students, int studentSize);

void
sortingStudentVerbal(student_t *students, int studentSize);

int
makePlacementQuantative(student_t *students, int studentSize,
        universty_t *universities, int universitiesSize);

void
resetDepartmentSize(universty_t *universities, int uniSize);

int
isDepartment(department_t *department, int target, int depSize);

void
printTitle();

void
cleanDisplay();

long long int
getTCNoFromUser();

void
displayScoreOfGivenTC(student_t *students, int stuSize,
        universty_t *university, int uniSize);

void
displayScoreOfGivenName(student_t *students, int stuSize,
        universty_t *university, int uniSize);

#endif	/* TALAYHAN_101044044_H */


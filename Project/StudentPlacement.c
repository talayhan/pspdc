/*################################################################*/
/*                           Includes                             */
/*################################################################*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "101044044_Samet.h"

/*
* readStudentsScoresFromFile() Function
* ---------------------------------
* FILE *inputStudentsScore, / File pointer - 
* student_t *students, / Output-Argument array of structure type of student_t
* int studentSize      / Local variable keep Student size,
* ----------------------------------------------------------
* This function assumptions that need at least one student information.
* If doesnt have this function does not work correctly.
*/
int
readStudentsScoresFromFile(FILE *inputStudentsScore, student_t *students, int studentSize)
{
	char garbage;
	int i = 0;

	/* Reads first row from file */
	while( garbage != '\n')
	{
		garbage = fgetc(inputStudentsScore);
		#ifdef DEBUG
			printf("%c",garbage);
		#endif
	}

	do
	{
		/* Reads ID Number */
		fscanf(inputStudentsScore,"%lld",&students[i].id);
		#ifdef DEBUG
			printf("Students[%d].id : %lld\n",i,students[i].id);
		#endif

		/* Read semicolon */
		garbage = fgetc(inputStudentsScore);

		/* Read Quantative Score */
		fscanf(inputStudentsScore,"%lf",&students[i].quantativeScore);
		#ifdef DEBUG
			printf("Students[%d].quantativeScore : %f\n",i,students[i].quantativeScore);
		#endif

		/* Read semicolon */
		garbage = fgetc(inputStudentsScore);

		/* Read Verbal Score */
		fscanf(inputStudentsScore,"%lf",&students[i].verbalScore);
		#ifdef DEBUG
			printf("Students[%d].verbalScore : %f\n\n",i,students[i].verbalScore );
		#endif
		++i;	/* Increment student subscript */
		/* Control student size, if file over MAX_STUDENT_SIZE, print ERROR */
		/* and break the loop. */
		if (i >= studentSize)
		{
			perror("File contains much more student!\n");
			perror("ERROR Code - 'ArrayOutOfBoundsException'\n");
			break;
		}
	}while(fgetc(inputStudentsScore) != EOF); /* Check - EOF */

	/* Return readed the number of students */
	return (i-1);

		/* Reads IDNumber part 
	garbage = fgetc(inputStudentsScore);
	while( garbage != ';')
	{
		students[0].id[i] = garbage;
		printf("%c",garbage);
		garbage = fgetc(inputStudentsScore);
		++i;
	}
	#ifndef DEBUG
		printf("Kac haneli oldugu: i = %d\n",i);
		printf("students[0].id = %s\n", students[0].id);
	#endif

	*/

	/*
	printf("\n\n0 atanmadan onceki deeeri: %c\n\n",garbage);
	/* Reads Quantative Score part 
	garbage = '0';
	i = 0;
	while( garbage != ';')
	{
		
		garbage = fgetc(inputStudentsScore);
		printf("%c",garbage);
		temp[i] = garbage;
		++i;
	}
	temp[i-1] = '\0';
	printf("temp'in atofa gitmeden son hali: %s\n",temp);
	students[0].quantativeScore = atof(temp);
	#ifndef DEBUG
		printf("Kac haneli oldugu: i = %d\n",i);
		printf("students[0].quantativeScore = %f\n", students[0].quantativeScore);
	#endif

	*/

	/* Clear temp 
	for (i = 0; i < 7; ++i)
	{
		temp[i] = '\0';
	}

	printf("%s\n",temp);

	/* Reads Verbal Score part 
	garbage = fgetc(inputStudentsScore);
	i = 0;
	while( garbage != '\n')
	{
		temp[i] = garbage;
		printf("%c",garbage);
		garbage = fgetc(inputStudentsScore);
		++i;
	}
	students[0].verbalScore = atof(temp);
	#ifndef DEBUG
		printf("Kac haneli oldugu: i = %d\n",i);
		printf("students[0].verbalScore = %f\n", students[0].verbalScore);
	#endif
	
    */
}

/*
* readStudentsNamesFromFile() Function
* ---------------------------------
* FILE *inputStudentsScore, / File pointer - 
* student_t *students, / Output-Argument array of structure type of student_t
* int studentSize      / Local variable keep Student size,
* ----------------------------------------------------------
* This function assumptions that need at least one student information.
* If doesnt have this function does not work correctly.
*/
void
readStudentsNamesFromFile(FILE *inputStudentName, student_t *students, int studentSize)
{
	long long int temp;	/* Keep student ID */
	String tempName;
	char garbage;
	int i = 0;	/* keep readed student size from file */
	int j = 0;	/* */

	/* Reads first row from file */
	while( garbage != '\n')
	{
		garbage = fgetc(inputStudentName);
		printf("%c",garbage);
	}

	do
	{
		/* Reads ID Number */
		fscanf(inputStudentName,"%lld",&temp);
		#ifdef DEBUG
			printf("Students[%d].id = temp : %lld\n",i,temp);
		#endif

		/* Read semicolon */
		garbage = fgetc(inputStudentName);
		garbage = ' ';	/* to enter the while loop */
		j = 0;
		/* Read student first name */
		while( garbage != ';')
		{
			garbage = fgetc(inputStudentName);
			tempName[j] = garbage;
			++j;
		}
		tempName[j-1] = '\0';
		strcpy(students[i].name,tempName);

		garbage = ' ';	/* to enter the while loop */
		j = 0;
		/* Read student sirname */
		while( garbage != ';')
		{
			garbage = fgetc(inputStudentName);
			tempName[j] = garbage;
			++j;
		}
		tempName[j-1] = '\0';
		strcpy(students[i].sirName,tempName);
		#ifdef DEBUG
			printf("%s\n", students[i].sirName);
		#endif

		/* Read student birthday day */
		fscanf(inputStudentName,"%d",&students[i].birthday.day);
		#ifdef DEBUG
			printf("Students[%d].birthday.day : %d\n",i,students[i].birthday.day);
		#endif

		/* Read birthday representation point */
		garbage = fgetc(inputStudentName);
		/* Read student birthday month */
		fscanf(inputStudentName,"%d",&students[i].birthday.month);
		#ifdef DEBUG
			printf("Students[%d].birthday.month : %d\n",i,students[i].birthday.month);
		#endif

		/* Read birthday representation point */
		garbage = fgetc(inputStudentName);
		/* Read student birthday year */
		fscanf(inputStudentName,"%d",&students[i].birthday.year);
		#ifdef DEBUG
			printf("Students[%d].birthday.year : %d\n",i,students[i].birthday.year);
		#endif

		/* Read semicolon */
		garbage = fgetc(inputStudentName);

		/* Read student score type */
		garbage = fgetc(inputStudentName);
		students[i].scoreType = garbage;
		#ifdef DEBUG
			printf("scoreType: %c\n\n\n", students[i].scoreType);
		#endif

		++i;	/* Increment student subscript */
		/* Control student size, if file over MAX_STUDENT_SIZE, print ERROR */
		/* and break the loop. 
		if (i > studentSize)
		{
			perror("File contains much more student!\n");
			perror("ERROR Code - 'ArrayOutOfBoundsException'\n");
			break;
		}
		*/
	}while(i < studentSize);
}
/*
* readStudentsAddressFromFile() Function
* ---------------------------------
* FILE *inputStudentsScore, / File pointer - 
* student_t *students, / Output-Argument array of structure type of student_t
* int studentSize      / Local variable keep Student size,
* ----------------------------------------------------------
* This function assumptions that need at least one student information.
* If doesnt have this function does not work correctly.
*/
void
readStudentsAddressFromFile(FILE *inputStudentAddress, student_t *students, int studentSize)
{
	long long int temp;	/* Keep student ID */
	String tempName;
	char garbage;
	int i = 0;	/* keep readed student size from file */
	int j = 0;	/* */

	/* Reads first row from file */
	while( garbage != '\n')
	{
		garbage = fgetc(inputStudentAddress);
		printf("%c",garbage);
	}

	for(i=0; i < studentSize; ++i)
	{
		/* Reads ID Number */
		fscanf(inputStudentAddress,"%lld",&temp);
		#ifdef DEBUG
			printf("Students[%d].id = temp : %lld\n",i,temp);
		#endif

		/* Read semicolon */
		garbage = fgetc(inputStudentAddress);

		/* Reads phone number */
		fscanf(inputStudentAddress,"%lld",&students[i].address.phone);
		#ifdef DEBUG
			printf("Students[%d].address.phone : %lld\n",i,students[i].address.phone);
		#endif

		/* Read semicolon */
		garbage = fgetc(inputStudentAddress);
		garbage = ' ';	/* to enter the while loop */
		j = 0;
		/* Read student e-mail address */
		while( garbage != ';')
		{
			garbage = fgetc(inputStudentAddress);
			tempName[j] = garbage;
			++j;
		}
		tempName[j-1] = '\0';
		strcpy(students[i].address.eMail,tempName);
		#ifdef DEBUG
			printf("%s\n", students[i].address.eMail);
		#endif

		garbage = ' ';	/* to enter the while loop */
		j = 0;
		/* Read student address of home */
		while( garbage != '\n')
		{
			garbage = fgetc(inputStudentAddress);
			tempName[j] = garbage;
			++j;
		}
		tempName[j-1] = '\0';
		strcpy(students[i].address.address,tempName);
		#ifdef DEBUG
			printf("%s\n\n", students[i].address.address);
		#endif
	}
}
void
readStudentsPreferencesFromFile(FILE *inputStudentsPreferences,
								student_t *students, int studentSize)
{
	long long int temp;	/* Keep student ID */
	String tempName;
	char garbage;
	char forwardPref;
	int i = 0;	/* keep readed student size from file */
	int j = 0;	/* */
	int k = 0;
	int status;

	/* Reads first row from file */
	while( garbage != '\n')
	{
		garbage = fgetc(inputStudentsPreferences);
		printf("%c",garbage);
	}

	for (i = 0; i < studentSize; ++i)
	{
		/* Reads ID Number */
		fscanf(inputStudentsPreferences,"%lld",&temp);
		#ifndef DEBUG
			printf("Students[%d].id = temp : %lld\n",i,temp);
		#endif

		for (j = 0; j < 8; ++j)
		{

			/* Read semicolon 
			garbage = fgetc(inputStudentsPreferences);
			fscanf(inputStudentsPreferences,"%d",&k);
			status = printf("K degeri : %d",k);
			if (status <= 0 )
			{
				printf("Düzgün Okuma gerceklesmedi!\n");
				/* Student has not preferences more. 
				while(fgetc(inputStudentsPreferences) != '\n')
				{/* intentionally empty }
			}

			students[i].preference.preferencesUniID[j] = k;

			*/



			/* Read semicolon */
			garbage = fgetc(inputStudentsPreferences);
			forwardPref = fgetc(inputStudentsPreferences);
			if (garbage == ';' && forwardPref == ';')
			{
				/* Student has not preferences more. */
				while(fgetc(inputStudentsPreferences) != '\n')
				{/* intentionally empty */}
				break;
			}
			else
			{
				students[i].preference.preferencesUniID[j] = (forwardPref - 48);
				printf("preferencesUniID forwardPref: %d\n", (forwardPref - 48) );

				/* Read semicolon */
				garbage = fgetc(inputStudentsPreferences);

				forwardPref = fgetc(inputStudentsPreferences);
				students[i].preference.preferencesDepID[j] = (forwardPref - 48);
				printf("preferencesDepID forwardPref: %d\n", (forwardPref - 48) );
			}
			
		}
	}

}
int
isElement(student_t *students, long long int target, int studentSize)
{
	int i;

	/* Students array if contains target return 1, other 0 */
	for (i = 0; i < studentSize; ++i)
	{
		if (students[i].id == target)
		{
			return 1;
		}
	}
	/* Array not contains target */
	return 0;
}

int
main(int argc, char const *argv[])
{
	FILE *inputStudentsScore;
	FILE *inputStudentName;
	FILE *inputStudentAddress;
	FILE *inputStudentsPreferences;
	int i,j;
	int totalStudentSize = 0;

	student_t students[MAX_STUDENT_SIZE];
	universty_t universities[MAX_UNI_SIZE];


	inputStudentsScore = fopen("Scores.csv","r");
	totalStudentSize = readStudentsScoresFromFile(inputStudentsScore,
											students, MAX_STUDENT_SIZE);
	fclose(inputStudentsScore);
	/* Debug Student Score information */
	#ifdef DEBUG
		/* Debug Code */
		for ( i = 0; i < 12; ++i)
		{
			printf("Students[%d].id : %lld\n",i,students[i].id );
			printf("Students[%d].quantativeScore : %f\n",i,students[i].quantativeScore);
			printf("Students[%d].verbalScore : %f\n",i,students[i].verbalScore );
		}
	#endif

	/* Test isElement Function. */
	#ifdef DEBUG
		if (isElement(students,students[3].id,MAX_STUDENT_SIZE))
		{
			printf("students[3].id is in the array. %lld\n",students[3].id );
		}
	#endif

	inputStudentName = fopen("StudentsNames.csv","r");
	readStudentsNamesFromFile(inputStudentName, students, totalStudentSize);
	fclose(inputStudentName);
	/* Debug Students personal information */
	#ifdef DEBUG
		/* Debug Code */
		for ( i = 0; i < 12; ++i)
		{
			printf("Students[%d].name : %s\n",i,students[i].name );
			printf("Students[%d].sirName : %s\n",i,students[i].sirName);
			printf("Students[%d].scoreType : %c\n",i,students[i].scoreType );
			printf("Students[%d].birthday.day : %d\n",i,students[i].birthday.day );
			printf("Students[%d].birthday.month : %d\n",i,students[i].birthday.month );
			printf("Students[%d].birthday.year : %d\n\n",i,students[i].birthday.year );
		}
	#endif


	inputStudentAddress = fopen("StudentsAddressInfo.csv","r");
	readStudentsAddressFromFile(inputStudentAddress, students, totalStudentSize);
	fclose(inputStudentAddress);
	/* Debug Students address information */
	#ifdef DEBUG
		/* Debug Code */
		for ( i = 0; i < 12; ++i)
		{
			printf("Students[%d].address.phone : %lld\n",i,students[i].address.phone );
			printf("Students[%d].address.eMail : %s\n",i,students[i].address.eMail );
			printf("Students[%d].address.address : %s\n\n",i,students[i].address.address );
			
		}
	#endif

	inputStudentsPreferences = fopen("Preferences.csv","r");
	readStudentsPreferencesFromFile(inputStudentsPreferences, students, totalStudentSize);
	fclose(inputStudentsPreferences);
	/* Debug Students preferences */
	#ifdef DEBUG
		/* Debug Code */
		for ( i = 0; i < 12; ++i)
		{
			for (j = 0; j < 8; ++j)
			{
				printf("Students[%d].preference.preferencesUniID[%d] : %d -- "
					,i,j,students[i].preference.preferencesUniID[j] );

				printf("Students[%d].preference.preferencesDepID[%d] : %d\n"
					,i,j,students[i].preference.preferencesDepID[j] );

			}
			printf("\n");
		}
	#endif





	return 0;
}
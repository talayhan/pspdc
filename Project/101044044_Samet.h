/*################################################################*/
/*                           Defines                              */
/*################################################################*/
#define DEBU

#define MAX_NAME_SIZE 50
#define MAX_UNI_SIZE 20
#define MAX_STUDENT_SIZE 50
#define PREFENCES_SIZE 8

typedef char String[MAX_NAME_SIZE];

typedef struct {
	int day;
	int month;
	int year;
} birthday_t;

typedef struct{
	String eMail;
	String address;
	long long int phone;
} address_t;

typedef struct {
	int preferencesUniID[PREFENCES_SIZE];
	int preferencesDepID[PREFENCES_SIZE];
	/* If placement UniID is -1, student is not be placement */
	int placementUniID;	
	int placementDepID;
} prefences_t;

typedef struct {
	long long int id;
	String name;
	String sirName;
	char scoreType;		/* Quantative or Verbal */
	birthday_t birthday;
	double quantativeScore;
	double verbalScore;
	address_t address;
	prefences_t preference;
} student_t;

typedef struct {
	String departmentName;
	int depID;
	char scoreType;
	int quota;
	int flag; /* 0 - 1 if is 0 not suitable to use */
} department_t;

typedef struct {
	String name;
	int uniID;
	department_t departments[MAX_NAME_SIZE];
} universty_t;
/* 
 * File:   main.c
 * Author: neo
 *
 * Created on May 19, 2013, 12:30 PM
 */

/*################################################################*/
/*                           Includes                             */
/*################################################################*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "Talayhan_101044044.h"


void
displayAllUniQuotas(universty_t *universities, int universitiesSize)
{
    int totalQuota = 0;
    int depSize;
    int i, j;
    
    for(i = 0; i < universitiesSize; ++i )
    {
        totalQuota = 0;
        for(j = 0; j < universities[i].departmentSize; ++j )
        {
            totalQuota += universities[i].departments[j].quota;
        }
        printf("%s Total Quota :[%d]\n",universities[i].name,totalQuota);
    }
}

void
displayUniStatistics(universty_t universities, int universitiesSize)
{
    int depSize = 0;
    int i = 0;
}


int
main(int argc, char const *argv[]) {
    FILE *inputStudentsScore;
    FILE *inputStudentName;
    FILE *inputStudentAddress;
    FILE *inputStudentsPreferences;
    FILE *inpUniDepInf;
    
    FILE *StudentDataBase;
    FILE *UniversityDataBase;
    
    int menuChoose = -1;
    int exitMenu = FALSE;
    int i, j;
    
    int studentSize = 0;
    student_t students[MAX_STUDENT_CAPACITY];
    int universitiesSize = 0;
    universty_t universities[MAX_UNI_CAPACITY];
   
    StudentDataBase = fopen(STUDENT_BIN,"rb");
    if(StudentDataBase == NULL)
    {
        /* read students score information */
        inputStudentsScore = fopen("Scores.csv", "r");
        if(inputStudentsScore == NULL)
            printf("Error file is not open!\n");
        studentSize = readStudentsScoresFromFile(inputStudentsScore,
                students, MAX_STUDENT_CAPACITY);
        fclose(inputStudentsScore);

        /* read students name */
        inputStudentName = fopen("StudentsNames.csv", "r");
        if(inputStudentName == NULL)
            printf("Error file is not open!\n");
        readStudentsNamesFromFile(inputStudentName, students, studentSize);
        fclose(inputStudentName);

        /* read students address */
        inputStudentAddress = fopen("StudentsAddressInfo.csv", "r");
        if(inputStudentAddress == NULL)
            printf("Error file is not open!\n");
        readStudentsAddressFromFile(inputStudentAddress, students, studentSize);
        fclose(inputStudentAddress);

        /* read students preferences */
        inputStudentsPreferences = fopen("Preferences.csv", "r");
        if(inputStudentsPreferences == NULL)
            printf("Error file is not open!\n");
        readStudentsPreferencesFromFile(inputStudentsPreferences, students);
        fclose(inputStudentsPreferences);
    }
    else
    {
        fread(&studentSize, sizeof(int), 1 ,StudentDataBase);
        fread(students,sizeof(student_t), studentSize ,StudentDataBase);
        fclose(StudentDataBase);
    }
    
    UniversityDataBase = fopen(UNIVERSITY_BIN,"rb");
    if(UniversityDataBase == NULL)
    {
        /* read departments informations */
        inpUniDepInf = fopen("Departments.csv","r");
        if(inpUniDepInf == NULL)
            printf("Error file is not open!\n");
        universitiesSize = readDepartments(inpUniDepInf, universities);
        fclose(inpUniDepInf);
    }
    else
    {
        fread(&universitiesSize, sizeof(int), 1, UniversityDataBase );
        fread(universities, sizeof(universty_t), universitiesSize ,UniversityDataBase);
        fclose(UniversityDataBase);
    }

    
    /* Debug Student Score information */
#ifdef DEBUG
    /* Debug Code */
    for (i = 0; i < 12; ++i) {
        printf("Students[%d].id : %lld\n", i, students[i].id);
        printf("Students[%d].quantativeScore : %f\n", i, students[i].quantativeScore);
        printf("Students[%d].verbalScore : %f\n\n", i, students[i].verbalScore);
    }
#endif

    /* Test isStudent Function. */
#ifdef DEBUG
    if (isStudent(students, students[3].id, MAX_STUDENT_CAPACITY)) {
        printf("students[3].id is in the array. %lld\n", students[3].id);
    }
#endif


    /* Debug Students personal information */
#ifdef DEBUG
    /* Debug Code */
    for (i = 0; i < 12; ++i) {
        printf("Students[%d].name : %s\n", i, students[i].name);
        printf("Students[%d].sirName : %s\n", i, students[i].sirName);
        printf("Students[%d].scoreType : %c\n", i, students[i].scoreType);
        printf("Students[%d].birthday.day : %d\n", i, students[i].birthday.day);
        printf("Students[%d].birthday.month : %d\n", i, students[i].birthday.month);
        printf("Students[%d].birthday.year : %d\n\n", i, students[i].birthday.year);
    }
#endif



    /* Debug Students address information */
#ifdef DEBUG
    /* Debug Code */
    for (i = 0; i < 12; ++i) {
        printf("Students[%d].address.phone : %lld\n", i, students[i].address.phone);
        printf("Students[%d].address.eMail : %s\n", i, students[i].address.eMail);
        printf("Students[%d].address.address : %s\n\n", i, students[i].address.address);

    }
#endif

    
    /* Debug Students preferences */
#ifdef DEBUG
    /* Debug Code */
    for (i = 0; i < 12; ++i) {
        for (j = 0; j < 8; ++j) {
            printf("Students[%d].preference.preferencesUniID[%d] : %d -- "
                    , i, j, students[i].preference.preferencesUniID[j]);

            printf("Students[%d].preference.preferencesDepID[%d] : %d\n"
                    , i, j, students[i].preference.preferencesDepID[j]);

        }
        printf("\n");
    }
#endif
    
    /* Debug Departments informations */
#ifdef DEBUG
    for (i = 0; i < 6; ++i)
    {
        printf("Universities[%d].name: %s \n",i,universities[i].name);
        for ( j = 0; j < 5; ++j)
        {
            printf("%s ---",
                universities[i].departments[j].departmentName);
            printf("%d ---",
                universities[i].departments[j].depID);
            printf("%d ---",
                universities[i].departments[j].quota);
            printf("%c \n",
                universities[i].departments[j].scoreType);
        }
    }
#endif
    
    
#ifdef DEBUG
    displayStudents(students, studentSize, 0);
    displayUniversities(universities, universitiesSize);
#endif
    
    resetDepartmentSize(universities, universitiesSize);
    sortingStudentQuantative(students,studentSize);
    i = makePlacementQuantative(students,studentSize,universities,universitiesSize);
    
    j=0;
    sortingStudentVerbal(students,studentSize);
    j = makePlacementVerbal(students,studentSize,universities,universitiesSize);
    
    do
    {
        printTitle();
        fputs("[0] - DISPLAY STUDENT INFO OF A GIVEN NAME AND SIRNAME\n",stdout);
        fputs("[1] - DISPLAY STUDENT INFO OF A GIVEN TC NO\n",stdout);
        fputs("[2] - DISPLAY ALL UNI QUOTAS\n",stdout);
        fputs("[3] - SHOW UNIVERSITIES INFORMATION\n",stdout);
        fputs("[4] - SHOW UNIVERSITIES PLACEMENT STATISTICS\n",stdout);
        fputs("[6] - Displaying score of a given TCKNo\n",stdout);
        fputs("[7] - DISPLAY ALL UNI QUOTA\n",stdout);
        fputs("[] - EXIT\n",stdout);
        fputs("[] - SAVE\n",stdout);
        scanf("%d",&menuChoose);
        cleanDisplay();
        switch(menuChoose)
        {
            case 0:
                displayScoreOfGivenName(students,studentSize,
                        universities,universitiesSize);
                break;
            case 1:
                displayScoreOfGivenTC(students,studentSize,
                        universities,universitiesSize);
                break;
            case 2:
                displayAllUniQuotas(universities,universitiesSize);
                break;
            case 3:
                displayUniversities(universities,universitiesSize);
                break;
            case 4:
                exitMenu = TRUE;
                break;
            case 5:
                exitMenu = TRUE;
                break;
            case 6:
                exitMenu = TRUE;
                break;
            case 7:
                displayAllUniQuotas(universities,universitiesSize);
                break;
            default:
                cleanDisplay();
                fputs("Enter valid menu number!>\n",stdout);
        }
    }while(!exitMenu);
    
    /* create binary file that keep students informations */
    StudentDataBase = fopen(STUDENT_BIN,"wb");
    fwrite(&studentSize, sizeof(int), 1, StudentDataBase );
    fwrite(students, sizeof(student_t) , studentSize, StudentDataBase );
    fclose(StudentDataBase);
    
    /* create binary file that keep universities informations */
    UniversityDataBase = fopen(UNIVERSITY_BIN,"wb");
    fwrite(&universitiesSize, sizeof(int), 1, UniversityDataBase);
    fwrite(universities, sizeof(universty_t) , universitiesSize , UniversityDataBase);
    fclose(UniversityDataBase);
    
    return 0;
}


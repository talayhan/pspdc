#include <stdio.h>
#include <string.h>
#include "Talayhan_101044044.h"
/**
 * 
 * @param inp
 * @param university
 * @return 
 */
int
readDepartments(FILE *inp, universty_t *university)
{
    char garbage = ' ';
    char temp;
    int j = 0; /* number of departments */
    int k = 0; /* number of universities */
    int i = 0;
    int controlUni = FALSE;
    String tempStr;
    
    /* read first line from file, not used */
    while(garbage != '\n')
    {
        garbage = fgetc(inp);
    }
    
    do
    {
        i = 0;
        temp = ' ';
        while(temp != ';') /* read University */
        {
            temp = fgetc(inp);
            tempStr[i++] = temp;
        }
        tempStr[--i] = '\0';
    #ifdef DEBUG
        fputs(tempStr,stdout);
    #endif
        if(strcmp(tempStr,university[k].name) == 0)
        {
            if(controlUni == TRUE)
            {
                /* same university but different department */
                j++; /* increase department count */
                university[k].departmentSize = (j+1);
            }
        }
        else
        {
            if(controlUni == TRUE)
            {
                /* assign department size */
                university[k].departmentSize = (j+1);
                k++; /* increase university count */
                j = 0; /* reset department count */
            }
        }
        strcpy(university[k].name,tempStr);
        /* read UniID */
        fscanf(inp,"%d;",&university[k].uniID);

        /* read Department */
        i = 0;
        temp = ' ';
        while(temp != ';')
        {
            temp = fgetc(inp);
            tempStr[i++] = temp;
        }
        tempStr[--i] = '\0';
    #ifdef DEBUG
        fputs(tempStr,stdout);
    #endif
        strcpy(university[k].departments[j].departmentName,tempStr);
        /* read DepID */
        fscanf(inp,"%d;",&university[k].departments[j].depID);

        garbage = fgetc(inp); /* read Score Type */
        
        /* Department Score Type */
        university[k].departments[j].scoreType = garbage;

        garbage = fgetc(inp);/* read semicolon */
        
        /* read Quota */
        fscanf(inp,"%d",&university[k].departments[j].quota);
        controlUni = TRUE;
    }while(fgetc(inp) != EOF);
    
    return (k+1);
}
/**
 * 
 * @param students
 * @param target
 * @param studentSize
 * @return 
 */
int
isStudent(student_t *students, long long int target, int studentSize) {
    int i;

    /* Students array if contains target return index, other -1 */
    for (i = 0; i < studentSize; ++i) {
        if (students[i].id == target) {
            return i;
        }
    }
    /* Array not contains target */
    return -1;
}
/* 
 * Return True if university is in array, otherwise return False, */
/**
 * 
 * @param universties
 * @param uniID
 * @param uniSize
 * @return 
 */
int
isUniversity(universty_t *universties, int uniID, int uniSize)
{
    return uniSize > uniID;
}

/*
 * readStudentsAddressFromFile() Function
 * ---------------------------------
 * FILE *inputStudentsScore, / File pointer - 
 * student_t *students, / Output-Argument array of structure type of student_t
 * int studentSize      / Local variable keep Student size,
 * ----------------------------------------------------------
 * This function assumptions that need at least one student information.
 * If doesnt have this function does not work correctly.
 */
void
readStudentsAddressFromFile(FILE *inputStudentAddress, student_t *students, 
                                int studentSize)
{
    long long int temp; /* Keep student ID */
    String tempName;
    char garbage;
    int i = 0; /* keep readed student size from file */
    int j = 0; /* */
    
    /* Reads first row from file */
    while (garbage != '\n') {
        garbage = fgetc(inputStudentAddress);
    }

        /* Reads ID Number */
    while(fscanf(inputStudentAddress, "%lld", &temp))
    {
#ifdef DEBUG
        printf("Students[%d].id = temp : %lld\n", i, temp);
#endif

        /* Read semicolon */
        garbage = fgetc(inputStudentAddress);

        /* Reads phone number */
        fscanf(inputStudentAddress, "%lld", &students[i].address.phone);
#ifdef DEBUG
        printf("Students[%d].address.phone : %lld\n", i, students[i].address.phone);
#endif

        /* Read semicolon */
        garbage = fgetc(inputStudentAddress);
        garbage = ' '; /* to enter the while loop */
        j = 0;
        /* Read student e-mail address */
        while (garbage != ';') {
            garbage = fgetc(inputStudentAddress);
            tempName[j] = garbage;
            ++j;
        }
        tempName[j - 1] = '\0';
        strcpy(students[i].address.eMail, tempName);
#ifdef DEBUG
        printf("%s\n", students[i].address.eMail);
#endif

        /* read student address of home */
        fgets(tempName,MAX_NAME_SIZE,inputStudentAddress);
        strcpy(students[i].address.address, tempName);
        
#ifdef DEBUG
        printf("%s\n\n", students[i].address.address);
#endif
        if(i == studentSize-1)
            break;
        else
            ++i;
    }
}

/*
 * readStudentsNamesFromFile() Function
 * ---------------------------------
 * FILE *inputStudentsScore, / File pointer - 
 * student_t *students, / Output-Argument array of structure type of student_t
 * int studentSize      / Local variable keep Student size,
 * ----------------------------------------------------------
 * This function assumptions that need at least one student information.
 * If doesnt have this function does not work correctly.
 */
void
readStudentsNamesFromFile(FILE *inputStudentName, student_t *students, 
        int studentSize) {
    long long int temp; /* Keep student ID */
    String tempName;
    char garbage;
    int i = 0; /* keep readed student size from file */
    int j = 0; /* */
    
    /* Reads first row from file */
    while (garbage != '\n') {
        garbage = fgetc(inputStudentName);
    }

    do {
        /* Reads ID Number */
        fscanf(inputStudentName, "%lld", &temp);
#ifdef DEBUG
        printf("Students[%d].id = temp : %lld\n", i, temp);
#endif

        /* Read semicolon */
        garbage = fgetc(inputStudentName);
        garbage = ' '; /* to enter the while loop */
        j = 0;
        /* Read student first name */
        while (garbage != ';') {
            garbage = fgetc(inputStudentName);
            tempName[j] = garbage;
            ++j;
        }
        tempName[j - 1] = '\0';
        strcpy(students[i].name, tempName);

        garbage = ' '; /* to enter the while loop */
        j = 0;
        /* Read student sirname */
        while (garbage != ';') {
            garbage = fgetc(inputStudentName);
            tempName[j] = garbage;
            ++j;
        }
        tempName[j - 1] = '\0';
        strcpy(students[i].sirName, tempName);
#ifdef DEBUG
        printf("%s\n", students[i].sirName);
#endif

        /* Read student birthday day */
        fscanf(inputStudentName, "%d", &students[i].birthday.day);
#ifdef DEBUG
        printf("Students[%d].birthday.day : %d\n", i, students[i].birthday.day);
#endif

        /* Read birthday representation point */
        garbage = fgetc(inputStudentName);
        /* Read student birthday month */
        fscanf(inputStudentName, "%d", &students[i].birthday.month);
#ifdef DEBUG
        printf("Students[%d].birthday.month : %d\n", i, students[i].birthday.month);
#endif

        /* Read birthday representation point */
        garbage = fgetc(inputStudentName);
        /* Read student birthday year */
        fscanf(inputStudentName, "%d", &students[i].birthday.year);
#ifdef DEBUG
        printf("Students[%d].birthday.year : %d\n", i, students[i].birthday.year);
#endif

        /* Read semicolon */
        garbage = fgetc(inputStudentName);

        /* Read student score type */
        garbage = fgetc(inputStudentName);
        students[i].scoreType = garbage;
#ifdef DEBUG
        printf("scoreType: %c\n\n\n", students[i].scoreType);
#endif

        ++i; /* Increment student subscript */
    } while (i < studentSize);
}

/*
 * readStudentsScoresFromFile() Function
 * ---------------------------------
 * FILE *inputStudentsScore, / File pointer - 
 * student_t *students, / Output-Argument array of structure type of student_t
 * int studentSize      / Local variable keep Student size,
 * ----------------------------------------------------------
 * This function assumptions that need at least one student information.
 * If doesnt have this function does not work correctly.
 */
int
readStudentsScoresFromFile(FILE *inputStudentsScore, student_t *students, 
        int maxStudentsCap) {
    char garbage;
    int i = 0;

    /* Reads first row from file */
    while (garbage != '\n') {
        garbage = fgetc(inputStudentsScore);
    }

    do {
        /* Reads ID Number */
        fscanf(inputStudentsScore, "%lld", &students[i].id);
#ifdef DEBUG
        printf("Students[%d].id : %lld\n", i, students[i].id);
#endif

        /* Read semicolon */
        garbage = fgetc(inputStudentsScore);

        /* Read Quantative Score */
        fscanf(inputStudentsScore, "%lf", &students[i].quantativeScore);
#ifdef DEBUG
        printf("Students[%d].quantativeScore : %f\n", i, students[i].quantativeScore);
#endif

        /* Read semicolon */
        garbage = fgetc(inputStudentsScore);

        /* Read Verbal Score */
        fscanf(inputStudentsScore, "%lf", &students[i].verbalScore);
#ifdef DEBUG
        printf("Students[%d].verbalScore : %f\n\n", i, students[i].verbalScore);
#endif
        ++i; /* Increment student subscript */
        /* Control student size, if file over MAX_STUDENT_SIZE, print ERROR */
        /* and break the loop. */
        if (i >= maxStudentsCap) {
            perror("File contains much more student!\n");
            perror("ERROR Code - 'ArrayOutOfBoundsException'\n");
            break;
        }
    } while (fgetc(inputStudentsScore) != EOF); /* Check - EOF */

    /* Return readed the number of students */
    return (i);
}
/**
 * 
 * @param inputStudentsPreferences
 * @param students
 */
void
readStudentsPreferencesFromFile(FILE *inputStudentsPreferences,
        student_t *students) {
    
    long long int temp; /* Keep student ID */
    String tempName;
    char garbage;
    char control;
    int i = 0; /* keep readed student size from file */
    int j = 0; /* */
    int k = 0;
    int exit = FALSE;
    int status;

    /* Reads first row from file */
    while (garbage != '\n') {
        garbage = fgetc(inputStudentsPreferences);
    }
    

    while(!exit)
    {
    
        /* Reads ID Number */
        fscanf(inputStudentsPreferences, "%lld", &temp);
#ifdef DEBUG
        printf("Students[%d].id = temp : %lld\n", i, temp);
#endif
        
        /* Read semicolon */
        garbage = fgetc(inputStudentsPreferences);

        for (j = 0; j < 8; ++j)
        {
            status = 1;
            k = 0;
            //printf("First Garbage : %c\n",garbage);
            status = fscanf(inputStudentsPreferences, "%d", &k);

            if (status <= 0) {
                //printf("Uni Okuma gerceklesmedi!\n");
                students[i].preference.preferencesUniID[j] = -1;
                /* Student has not preferences more. */
                do{
                    
                    control = fgetc(inputStudentsPreferences);
                    if(control == EOF)
                    {
                        exit = TRUE;
                        break;
                    }
                }while (control != '\n');
                break;
            } else { /* Student has preferences. */
                students[i].preference.preferencesUniID[j] = k;
                //printf("UniPreference degeri [%d] : %d\n", i, k);
                /* Read semicolon */
                garbage = fgetc(inputStudentsPreferences);
                //printf("uni sonrasi ; Garbage : %c\n",garbage);
                k = 0;
                status = fscanf(inputStudentsPreferences, "%d", &k);

                if (status <= 0) {
                    //printf("department Düzgün Okuma gerceklesmedi!\n");
                    students[i].preference.preferencesDepID[j] = -1;
                    /* Student has not preferences more. */
                    while (fgetc(inputStudentsPreferences) != '\n') {
                        /* intentionally empty */
                    }
                    break;
                } else {
                    students[i].preference.preferencesDepID[j] = k;
                    //printf("DepPreference degeri [%d] : %d\n", i, k);
                    /* Read semicolon */
                    garbage = fgetc(inputStudentsPreferences);
                    //printf("dep sonrasi Garbage : %c\n",garbage);
                }
            }
        }
        ++i;
    }    
}
/**
 * 
 * @param universities
 * @param uniSize
 */
void
displayPlacementStatisticsOfDeparments(universty_t *universities, int uniSize)
{
    int i = 0;
    int j = 0;
    
    printf("UniID\tUni Name\t\t\tDepID\tDep Name\tPlacement Size\tQuota\tMin. Score\n");
    printf("------------------------------------\n");
    for(i = 0; i < uniSize; ++i)
    {
        for(j= 0; j < universities[i].departmentSize; ++j )
        {
            printf("[%d]\t%-20s\t\t",universities[i].uniID,universities[i].name);
            printf("[%d]\t%-20s\t",universities[i].departments[j].depID,
                    universities[i].departments[j].departmentName);
            printf("%d\t",universities[i].departments[j].size);
            printf("%d\t\n",universities[i].departments[j].quota);
        }
    }
}
/**
 * 
 * @param student
 * @param studentSize
 * @param universities
 */
void
displayStudents(student_t *student, int studentSize, universty_t *universities)
{
    int i = 0;
    int depIndex = -1;
    
    printf("No\tSirname\t\tFirstName\tID Number:\tScore T\tQS\tVS\tP. Rank\n");
    printf("------------------------------------------------------\n");
    for(i = 0; i < studentSize; ++i)
    {
        printf("[%d]\t%-15s\t%-15s\t",i,student[i].sirName,student[i].name);
        printf("%11lld\t%c\t%.3f\t%.3f\t%3d\n",student[i].
                id,student[i].scoreType,
                student[i].quantativeScore,student[i].verbalScore,
                student[i].preference.rank);
        /* determine department index */
        depIndex = isDepartment( universities[student[i].
                preference.placementUniID].departments,
            student[i].preference.placementDepID,
            universities[student[i].preference.placementUniID].departmentSize);
        printf("Placement Uni: [%s]\tPlacement Dep: [%s]\n\n",
                universities[student[i].preference.placementUniID].name,
                universities[student[i].preference.placementUniID].
                departments[depIndex].departmentName);
        
    }
}
/**
 * 
 * @param universities
 * @param universitiesSize
 */
void
displayUniversities(universty_t *universities, int universitiesSize)
{
    int i = 0;
    int j = 0;
    
    printf("UniID\tUni Name\t\t\tDepID\tDep Name\t\tQuota\n");
    printf("------------------------------------\n");
    for(i = 0; i < universitiesSize; ++i)
    {
        for(j= 0; j < universities[i].departmentSize; ++j )
        {
            printf("[%d]\t%-20s\t\t",universities[i].uniID,universities[i].name);
            printf("[%d]\t%-20s\t",universities[i].departments[j].depID,
                    universities[i].departments[j].departmentName);
            printf("%d\n",universities[i].departments[j].quota);
        }
    }
}
/**
 * 
 * @param students
 * @param studentSize
 */
void
sortingStudentQuantative(student_t *students, int studentSize)
{
    int i;
    student_t temp;
    int swapped;
    
    do
    {
        swapped = FALSE;
        for(i = 1; i <= studentSize-1; ++i)
        {
            if( students[i-1].quantativeScore < students[i].quantativeScore )
            {
                temp = students[i];
                students[i] = students[i-1];
                students[i-1] = temp;
                swapped = TRUE;
            }
        }
        --studentSize;
    }while(swapped);
}
/**
 * 
 * @param students
 * @param studentSize
 */
void
sortingStudentVerbal(student_t *students, int studentSize)
{
    int i;
    student_t temp;
    int swapped;
    
    do
    {
        swapped = FALSE;
        for(i = 1; i <= studentSize-1; ++i)
        {
            if( students[i-1].verbalScore < students[i].verbalScore )
            {
                temp = students[i];
                students[i] = students[i-1];
                students[i-1] = temp;
                swapped = TRUE;
            }
        }
        --studentSize;
    }while(swapped);
}
/**
 * 
 * @param students
 * @param studentSize
 * @param universities
 * @param universitiesSize
 * @return Size of Placement Student
 */
int
makePlacementQuantative(student_t *students, int studentSize,
        universty_t *universities, int universitiesSize)
{
    int suitable = TRUE;
    int i = 0;
    int j = 0;
    int sizeOfPlacementStud = 0;
    
    int stuUniChoose = -1;
    int stuDepChoose = -1;
    
    int depID = -1;

    for (i = 0; i < studentSize; ++i) {

        if (students[i].scoreType != 'Q') {
            suitable = FALSE;
        }
        else
            suitable = TRUE;
        for (j = 0; j < PREFENCES_SIZE && suitable != FALSE; ++j) {

            stuUniChoose = students[i].preference.preferencesUniID[j];
            /* student didn't preference */
            if (stuUniChoose == -1) {
                /* student do not be placement */
                students[i].preference.placementUniID = -1;
                students[i].preference.placementDepID = -1;
                break;
            } else {
                stuDepChoose = students[i].preference.preferencesDepID[j];

                depID = isDepartment(universities[stuUniChoose].departments,
                       stuDepChoose, universities[stuUniChoose].departmentSize);

                /* Not found this department, student don't placement */
                if (depID == -1) {
                    students[i].preference.placementUniID = -1;
                } else {
                    /* Quota not full and department is suitable for student,
                     * student will be placement */
                    if ( (universities[stuUniChoose].departments[depID].size <
                            universities[stuUniChoose].departments[depID].quota)
                            && (universities[stuUniChoose].departments[depID].
                            scoreType != 'V') )
                    {
                        /* increase the department size */
                        universities[stuUniChoose].departments[depID].size++;
                        students[i].preference.rank =
                                universities[stuUniChoose].
                                departments[depID].size;
                        sizeOfPlacementStud++;

                        students[i].preference.placementUniID = stuUniChoose;
                        students[i].preference.placementDepID = stuDepChoose;
                        break;
                    } else {
                        students[i].preference.placementUniID = -1;
                        students[i].preference.placementDepID = -1;
                    }
                }
            }/* end preferences for */
        }/* end student for */
    }/* end if */ 
    return sizeOfPlacementStud;
}

/**
 * 
 * @param students
 * @param studentSize
 * @param universities
 * @param universitiesSize
 * @return Size of Placement Student
 */
int
makePlacementVerbal(student_t *students, int studentSize,
        universty_t *universities, int universitiesSize)
{
    int suitable = TRUE;
    int i = 0;
    int j = 0;
    int sizeOfPlacementStud = 0;
    
    int stuUniChoose = -1;
    int stuDepChoose = -1;
    
    int depID = -1;

    for (i = 0; i < studentSize; ++i) {

        if (students[i].scoreType != 'V') {
            suitable = FALSE;
        }
        else
            suitable = TRUE;
        for (j = 0; j < PREFENCES_SIZE && suitable != FALSE; ++j) {

            stuUniChoose = students[i].preference.preferencesUniID[j];
            /* student didn't preference */
            if (stuUniChoose == -1) {
                /* student do not be placement */
                students[i].preference.placementUniID = -1;
                students[i].preference.placementDepID = -1;
                break;
            } else {
                stuDepChoose = students[i].preference.preferencesDepID[j];

                depID = isDepartment(universities[stuUniChoose].departments,
                       stuDepChoose, universities[stuUniChoose].departmentSize);

                /* Not found this department, student don't placement */
                if (depID == -1) {
                    students[i].preference.placementUniID = -1;
                } else {
                    /* Quota not full and department is suitable for student,
                     * student will be placement */
                    if ( (universities[stuUniChoose].departments[depID].size <
                            universities[stuUniChoose].departments[depID].quota)
                            && (universities[stuUniChoose].departments[depID].
                            scoreType != 'Q') )
                    {
                        /* increase the department size */
                        universities[stuUniChoose].departments[depID].size++;
                        students[i].preference.rank =
                                universities[stuUniChoose].
                                departments[depID].size;
                        sizeOfPlacementStud++;

                        students[i].preference.placementUniID = stuUniChoose;
                        students[i].preference.placementDepID = stuDepChoose;
                        break;
                    } else {
                        students[i].preference.placementUniID = -1;
                        students[i].preference.placementDepID = -1;
                    }
                }
            }/* end preferences for */
        }/* end student for */
    }/* end if */ 
    return sizeOfPlacementStud;
}

/**
 * If target is department return [index], else return -1
 * @param department
 * @param target
 * @param depSize
 * @return 
 */
int
isDepartment(department_t *department, int target, int depSize)
{
    int i = 0;
    
    for(i = 0; i < depSize; ++i)
    {
        if(department[i].depID == target)
        {
            return i;
        }
    }
    
    return -1;
}

/**
 * This functions set to zero that the given department size.
 * @param universities
 * @param uniSize
 */
void
resetDepartmentSize(universty_t *universities, int uniSize)
{
    int i,j;
    
    for(i = 0; i < uniSize; ++i){
        for(j = 0; j < universities[i].departmentSize; ++j)
        {
            universities[i].departments[j].size = 0;
        }
    }
}
/**
 * Print the console program title.
 */
void
printTitle()
{       
    fputs("########################################################\n",stdout);
    fputs("--------------- Student Placement Automation -----------\n",stdout);
    fputs("########################################################\n",stdout);
    
    fputs("",stdout);
}
/**
 * Clean the console.
 */
void
cleanDisplay()
{
    int i = 0;
    for(i = 0; i < 30 ; ++i)
        printf("\n");
}
/**
 * 
 * @return Student TC no.
 */
long long int
getTCNoFromUser()
{
    long long int tcNo = 0;
    
    printf("Enter TC No: ");
    if(scanf("%lld",&tcNo))
    {
        /* intentionally empty */
    }else{
        printf("Error reading TC no!\n");
    }
    
    return tcNo;
}

/**
 * Print the console, student's scores and placement informations.
 * @param students
 * @param stuSize
 * @param university
 * @param uniSize
 */
void
displayScoreOfGivenTC(student_t *students, int stuSize,
        universty_t *university, int uniSize)
{
    int stuIndex;
    long long int studentTCNo;
    int depIndex;
    
    studentTCNo = getTCNoFromUser();
    
    stuIndex = isStudent(students, studentTCNo, stuSize);
    if( stuIndex == -1 )
    {
        printf("TC No:[%lld] is not valid!\n",studentTCNo);
    }
    else
    {
        printf("Student Name: %s\n", students[stuIndex].name);
        printf("Student Sirname: %s\n", students[stuIndex].sirName);
        printf("Quantative Score: [%.3f]\n",
                students[stuIndex].quantativeScore);
        printf("Verbal Score: [%.3f]\n",
                students[stuIndex].verbalScore);
        if( students[stuIndex].preference.placementUniID == -1 )
        {
            printf("Student doesn't be placement!\n");
        }
        else
        {
            depIndex = isDepartment(
            university[students[stuIndex].preference.placementUniID].
                    departments,
                    students[stuIndex].preference.placementDepID,
            university[students[stuIndex].preference.placementUniID].
                    departmentSize);
            printf("Student Placement University: %s\n",
                    university[students[stuIndex].preference.
                    placementUniID].name );
            printf("Student Placement Department: %s\n",
            university[students[stuIndex].preference.placementUniID].
                    departments[depIndex].departmentName);
        }
    }
}

/**
 * 
 * @param students
 * @param stuSize
 * @param university
 * @param uniSize
 */
void
displayScoreOfGivenName(student_t *students, int stuSize,
        universty_t *university, int uniSize)
{
    int stuIndex;
    int depIndex;
    long long int studentTCNo;
    int i = 0;
    
    String name;
    String sirName;
    
    printf("Enter Student Name: ");
    scanf("%s",name);
    
    printf("\nEnter Student Sirname: ");
    scanf("%s",sirName);
    
    for(i = 0; i < stuSize; ++i)
    {
        if( strcmp(students[i].name,name) == 0 )
        {
            studentTCNo = students[i].id;
            break;
        }
    }
    if(i >= stuSize)
    {
        printf("We dont have student, this name!\n");
    }
    
    stuIndex = isStudent(students, studentTCNo, stuSize);
    if( stuIndex == -1 )
    {
        /* intentionally empty! */
    }
    else
    {
        printf("Student Name: %s\n", students[stuIndex].name);
        printf("Student Sirname: %s\n", students[stuIndex].sirName);
        printf("Quantative Score: [%.3f]\n",
                students[stuIndex].quantativeScore);
        printf("Verbal Score: [%.3f]\n",
                students[stuIndex].verbalScore);
        if( students[stuIndex].preference.placementUniID == -1 )
        {
            printf("Student doesn't be placement!\n");
        }
        else
        {
            depIndex = isDepartment(
            university[students[stuIndex].preference.placementUniID].
                    departments,
                    students[stuIndex].preference.placementDepID,
            university[students[stuIndex].preference.placementUniID].
                    departmentSize);
            printf("Student Placement University: %s\n",
                    university[students[stuIndex].preference.
                    placementUniID].name );
            printf("Student Placement Department: %s\n",
            university[students[stuIndex].preference.placementUniID].
                    departments[depIndex].departmentName);
        }
    }
}
